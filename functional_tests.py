from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest

class MySeleniumTests(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Chrome()

    def tearDown(self):
        self.browser.close()

    def test_index_page(self):
        self.browser.get('http://localhost:8000')
        self.assertIn('My Blog', self.browser.title)
        self.fail('Finish the test!')

if __name__ == '__main__':
    unittest.main(warnings='ignore')


import pytest
from mixer.backend.django import mixer
pytestmark = pytest.mark.django_db

class TestPost:
    def test_model(self):
        post = mixer.blend('blog.Post')
        assert post.pk == 1, 'Should create a Post instance'

    def test_string_representation(self):
        post = mixer.blend('blog.Post', title='My first post')
        assert str(post) == 'My first post', 'Should return the correct string representation'


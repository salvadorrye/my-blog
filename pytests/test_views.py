from django.test import RequestFactory
from .. import views

class TestIndexView:
    def test_anonymous(self):
        req = RequestFactory().get('/')
        resp = views.index(req)
        assert resp.status_code == 200, 'Should be callable by anyone'

